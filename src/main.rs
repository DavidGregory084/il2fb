use classfile_parser::class_parser;
use classfile_parser::constant_info::*;
use classfile_parser::ClassFile;
use jni::objects::*;
use jni::*;
use rts::*;
use std::io::{Error, ErrorKind, Result};
use std::path::Path;

fn fetch_class_from_sfs(
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    class_name: String,
) -> Result<(Vec<u8>, ClassFile)> {
    let class_data = rts::unpack_from_sfs_by_class_name(sfs_file, decompressed, class_name)?;

    let (_, parsed_class): (&[u8], ClassFile) = class_parser(&class_data).map_err(|_| {
        return Error::new(ErrorKind::InvalidData, "Error parsing class file");
    })?;

    return Ok((class_data, parsed_class));
}

fn get_class_name(class_file: &ClassFile) -> Result<String> {
    let this_class_idx = class_file.this_class - 1;

    let name_index = match class_file.const_pool[this_class_idx as usize] {
        ConstantInfo::Class(ClassConstant { name_index }) => Ok(name_index),
        _ => Err(Error::new(
            ErrorKind::InvalidData,
            "No class file name constant found in the class file",
        )),
    }?;

    let class_name_idx = name_index - 1;

    let class_name = match &class_file.const_pool[class_name_idx as usize] {
        ConstantInfo::Utf8(Utf8Constant { utf8_string, .. }) => Ok(utf8_string),
        _ => Err(Error::new(
            ErrorKind::InvalidData,
            "Class file name constant not found at the declared index",
        )),
    }?;

    return Ok(class_name.to_string());
}

fn get_system_classloader<'a>(env: &'a JNIEnv) -> Result<JObject<'a>> {
    let loader_class = env
        .find_class("java/lang/ClassLoader")
        .expect("Unable to find Java ClassLoader class");

    let loader_value = env
        .call_static_method(
            loader_class,
            "getSystemClassLoader",
            "()Ljava/lang/ClassLoader;",
            &[],
        )
        .expect("Unable to get system class loader");

    return match loader_value {
        JValue::Object(jobject) => Ok(jobject),
        _ => Err(Error::new(
            ErrorKind::NotFound,
            "Unable to get system class loader",
        )),
    };
}

fn load_class_from_sfs<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
    class_name: String,
) -> Result<JClass<'a>> {
    let (class_data, parsed_class) = fetch_class_from_sfs(sfs_file, &decompressed, class_name)?;

    let class_name = get_class_name(&parsed_class)?;

    // Uncomment this code to write the class to disk
    //
    // let file_name = format!("{}.class", &class_name);
    // let file_path = Path::new(&file_name);
    // let file_dir = file_path.parent().unwrap();
    // std::fs::create_dir_all(file_dir)?;
    // std::fs::write(file_path, &class_data)?;

    println!("Loaded {}", &class_name);

    let class = env.define_class(&class_name, *loader, &class_data).unwrap();

    Ok(class)
}

fn load_file_from_sfs<'a>(
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    fingerprint: i32,
    xor_key: i32,
) -> Result<()> {
    let data =
        rts::unpack_from_sfs_by_fingerprint(sfs_file, decompressed, fingerprint, Some(xor_key))?;

    // Uncomment this code to write the file to disk
    //
    // let file_name = format!("cod/{}", fingerprint);
    // let file_path = Path::new(&file_name);
    // let file_dir = file_path.parent().unwrap();
    // std::fs::create_dir_all(file_dir)?;
    // std::fs::write(file_path, &data)?;

    return Ok(());
}

fn load_ldr_callback_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.LDRCallBack".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_ldr_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.LDR".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_finger_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.Finger".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_sfs_input_stream_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.SFSInputStream".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_rts_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.RTS".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_aircraft_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.il2.objects.air.Aircraft".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_bmputils_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.il2.engine.BmpUtils".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_fmmath_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.il2.fm.FMMath".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_joy_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.Joy".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_joydx_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.JoyDX".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_joyff_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.JoyFF".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_joywin_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.JoyWin".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_keyboarddx_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.KeyboardDX".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_keyboardwin_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.KeyboardWin".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_mainwin32_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.MainWin32".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_mainwindow_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.MainWindow".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_mousedx_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.MouseDX".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_mousewin_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.MouseWin".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_stringclipboard_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.StringClipboard".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn load_trackirwin_class<'a>(
    env: &'a JNIEnv,
    sfs_file: &SfsFile,
    decompressed: &Vec<u8>,
    loader: &'a JObject,
) -> Result<JClass<'a>> {
    let class_name = "com.maddox.rts.TrackIRWin".to_string();
    let class = load_class_from_sfs(env, sfs_file, decompressed, loader, class_name)?;
    env.new_global_ref(*class).unwrap();
    Ok(class)
}

fn create_new_instance<'a>(
    env: &'a JNIEnv,
    class: JClass<'a>,
    class_name: &str,
) -> Result<JObject<'a>> {
    return match env
        .call_method(*class, "newInstance", "()Ljava/lang/Object;", &[])
        .expect(format!("Unable to call newInstance method on {}", class_name).as_str())
    {
        JValue::Object(jobject) => Ok(jobject),
        _ => Err(Error::new(
            ErrorKind::InvalidData,
            format!("Unable to retrieve instance for {} object", class_name),
        )),
    };
}

fn main() -> std::io::Result<()> {
    let java_args: InitArgs = InitArgsBuilder::new()
        .version(JNIVersion::V8)
        .option("-Djava.class.path=.")
        .build()
        .unwrap();

    // Create the Java VM
    let java_vm = JavaVM::new(java_args).expect("Unable to create Java VM");

    let attach_guard = java_vm
        .attach_current_thread()
        .expect("Unable to attach main thread to Java VM");

    let env = &*attach_guard;

    // Load the files.sfs file
    let path = Path::new("C:\\Users\\David\\Documents\\files.414.SFS");
    let sfs_file = rts::read_sfs(path)?;
    let decompressed = rts::decompress_sfs(&sfs_file)?;

    // Start loading classes
    let system_loader = get_system_classloader(&env)?;

    // For the first few classes that are loaded, the game executable attaches a JVMPI event handler
    // that hooks the class load event.
    //
    // It has a different XOR table for each of these five classes. I think this was to make it especially
    // hard to gain access to these crucial classes.

    // First the com.maddox.rts.LDRCallBack class, which is used by...
    load_ldr_callback_class(&env, &sfs_file, &decompressed, &system_loader)?;

    // ...the com.maddox.rts.LDR class, the main classloader for the game
    let ldr_class = load_ldr_class(&env, &sfs_file, &decompressed, &system_loader)?;

    // We create an instance of the main classloader and pin it so it won't be GCed
    let ldr_instance = create_new_instance(env, ldr_class, "com/maddox/rts/LDR")?;
    env.new_global_ref(ldr_instance).unwrap();

    // Next we need to load the com.maddox.rts.Finger class, which does Rabin fingerprinting.
    //
    // This is essentially the core cryptographic primitive that is used throughout the game's code.
    //
    // The game's Rabin hashing algorithms seem to have been copied from this Modula-3 implementation:
    //   https://www.cs.tut.fi/lintula/manual/modula3/m3sources/html/m3tk/src/misc/FingerPrint.m3.html
    //
    // They even use the left-shifted values and the same names for the precomputed lookup tables (FPaTable, FPbTable).
    load_finger_class(&env, &sfs_file, &decompressed, &ldr_instance)?;

    // Now the com.maddox.rts.SFSInputStream class; this is used to load resources from the SFS files.
    let sfs_is_class = load_sfs_input_stream_class(&env, &sfs_file, &decompressed, &ldr_instance)?;

    // Finally the com.maddox.rts.RTS class is loaded.
    //
    // This contains methods for loading the native libraries used by the game code as well
    // as the actual implementation of the class loading code.
    //
    let rts_class = load_rts_class(&env, &sfs_file, &decompressed, &ldr_instance)?;

    // load_aircraft_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_bmputils_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_fmmath_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_joy_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_joydx_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_joyff_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_joywin_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_keyboarddx_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_keyboardwin_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_mainwin32_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_mainwindow_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_mousedx_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_mousewin_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_stringclipboard_class(&env, &sfs_file, &decompressed, &ldr_instance)?;
    // load_trackirwin_class(&env, &sfs_file, &decompressed, &ldr_instance)?;

    // We need to create an instance of this too and pin it
    let rts_instance = create_new_instance(env, rts_class, "com/maddox/rts/RTS")?;
    env.new_global_ref(rts_instance).unwrap();

    // Next we need to set the path to the native RTS library (used to be rts.dll) in the LDR class.
    //
    // This is important because LDR will load all of the game's classes, and it uses the parameter of this
    // method call to decide which library to load when a class calls `System.loadLibrary("rts");`
    //
    let current_exe_path =
        std::env::current_exe().expect("Unable to retrieve path of current executable");

    let current_exe_str = current_exe_path
        .to_str()
        .expect("Unable to convert current executable path to a valid UTF-8 string");

    let rts_lib_name = env
        .new_string(current_exe_str)
        .expect("Unable to create Java String");

    env.call_method(
        ldr_instance,
        "link",
        "(Ljava/lang/String;)V",
        &[JValue::Object(*rts_lib_name)],
    )
    .expect("Error calling com/maddox/rts/LDR#link method");

    env.call_static_method(sfs_is_class, "_loadNative", "()V", &[])
        .expect("Error calling com/maddox/SFSInputStream._loadNative");

    // Won't succeed until native methods are implemented
    // env.call_method(ldr_instance, "set", "(Ljava/lang/Object;)V", &[JValue::Object(rts_instance)]).expect("Error calling com/maddox/rts/LDR#set");

    // let main_class_str = "com.maddox.il2.game.GameWin3D";
    // let main_class_name = env.new_string(main_class_str).expect("Unable to create Java String");
    // let main_class =  match env.call_method(ldr_instance, "loadClass", "(Ljava/lang/String;)Ljava/lang/Class;", &[JValue::Object(*main_class_name)]).expect("Error calling com/maddox/rts/LDR#loadClass") {
    // 	JValue::Object(jobject) => Ok(jobject),
    // 	_ => Err(Error::new(ErrorKind::NotFound, "Unable to load main class com/maddox/il2/game/GameWin3D"))
    // }?;

    // Won't succeed until the LDR#set call above is working
    // env.call_static_method(main_class, "main", "([Ljava/lang/String)V", &[]).expect("Error calling com/maddox/il2/game/GameWin3D.main");

    if env.exception_check().unwrap() {
        env.exception_describe().unwrap()
    };

    return Ok(());
}
